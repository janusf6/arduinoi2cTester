#include <Arduino.h>
#include <Wire.h>
String inputString = "";
boolean stringComplete = false;
int baud=9600;
int address=6;
int lastMessage=0;
void wireListener(int byteCount);
void setup() {

    pinMode(A5,INPUT_PULLUP);
    if(digitalRead(A0)==HIGH)
    {
        address=8;
    }
    Wire.begin(address);
    Serial.begin(baud);
    inputString.reserve(200);

    Wire.onReceive(wireListener);
}

void loop() {

}
void transmitValue(int number)
{

    int16_t bigNum = number;
    byte myArray[2];
        if(address==6)
        {
            Wire.beginTransmission(8); // transmit to device #8
        }
    else
        {
            Wire.beginTransmission(6); // transmit to device #8
        }

    myArray[0] = (bigNum >> 8) & 0xFF;
    myArray[1] = bigNum & 0xFF;
    Wire.write(myArray, 2);

    Wire.write( '\n');
    Wire.endTransmission();

}


void wireListener(int byteCount) {

    int counter = 0;
    byte myArray[2];

    while (Wire.available() > 0) { // loop through all
        byte c = Wire.read(); // receive byte as a character
        if (counter < 2) {
            myArray[counter] = c;
        }
        counter++;

    }

    int result = myArray[0] << 8 | myArray[1];


    Serial.println(result);
    if(lastMessage!=result)
    {
        lastMessage=result;
        transmitValue(result);
    }
//     transmitValue(result);

}

void  clearInput() {
    inputString="";
    stringComplete=false;
}

void serialEvent() {

    while (Serial.available()) {
        // get the new byte:
        char inChar = (char)Serial.read();
        // add it to the inputString:
         inputString += inChar;
        // if the incoming character is a newline, set a flag
        // so the main loop can do something about it:
        if (inChar == '\n') {
            stringComplete = true;
            float val=0;
            val= inputString.toFloat();


            clearInput();
//            Serial.println(val);
            transmitValue(val);
        }

    }

}